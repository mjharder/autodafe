# Test makefile for makemake

# SPDX-FileCopyrightText: Copyright Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

# Use absolute path so tests that change working directory still use
# scripts from parent directory.  Note that using $PWD seems to fail
# here under Gitlab's CI environment.
PARDIR=$(realpath ..)
PATH := $(PARDIR):$(realpath .):${PATH}

# Find all *.mk entries to test
TESTLOADS := $(shell ls -1 *.mk | sed '/.mk/s///' | sort)

.PHONY: cHeck clean testlist listcheck buildchecks

check:
	@make tap | ./tapview

.SUFFIXES: .chk

clean:
	rm -fr *~

# Show summary lines for all tests.
testlist:
	@grep '^##' *.mk
listcheck:
	@for f in *.mk; do \
	    if ( head -3 $$f | grep -q '^ *##' ); then :; else echo "$$f needs a description"; fi; \
	done

# Rebuild characterizing tests
buildchecks:
	@for file in $(TESTLOADS); do \
	    echo "Remaking $${file}.chk"; \
	    OPTS=`sed -n /#options:/s///p <$${file}.mk`; \
	    makemake -q $$OPTS $${file}.mk >$${file}.chk 2>&1 || exit 1; \
	done;

RUN_TARGETS=$(TESTLOADS:%=run-regress-%)
$(RUN_TARGETS): run-regress-%: %.mk
	@(test=$(<:.mk=); legend=$$(sed -n '/^## /s///p' <"$<" 2>/dev/null || echo "(no description)"); \
	OPTS=`sed -n /#options:/s///p $<`; \
	makemake -q $$OPTS $< | ./tapdiffer "$${test}: $${legend}" "$${test}.chk")

IDEMPOTENCY_TARGETS=$(TESTLOADS:%=check-idempotency-%)
$(IDEMPOTENCY_TARGETS): check-idempotency-%: %.chk
	@(test=$(<:.chk=); legend=$$(sed -n '/^## /s///p' <"$<" 2>/dev/null || echo "(no description)"); \
	makemake -q $< | ./tapdiffer "$${test}: $${legend} idempotency" $<)

SHELL_LOADS := onepass
SHELL_TARGETS = $(SHELL_LOADS:%=shell-test-%)
$(SHELL_TARGETS): shell-test-%:
	@$(SHELL) $*.sh

amhello-normal:
	@./test_makemake amhello

amhello-undo:
	@./test_makemake -u amhello

giflib-normal:
	@./test_makemake giflib

PROJECT_TARGETS = amhello-normal amhello-undo giflib-normal

TEST_TARGETS = $(RUN_TARGETS) $(IDEMPOTENCY_TARGETS) $(SHELL_TARGETS) $(PROJECT_TARGETS)

tap: count $(TEST_TARGETS)
count:
	@echo 1..$(words $(TEST_TARGETS))

